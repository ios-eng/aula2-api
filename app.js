var bodyParser = require('body-parser')
var express = require('express');
var app = express();

// parse application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }))

// parse application/json
app.use(bodyParser.json())

var movies = [
    {
        id: 1,
        name: "Bulbasaur",
        image: "https://pokeres.bastionbot.org/images/pokemon/1.png"
    },
    {
        id: 2,
        name: "Ivesaur",
        image: "https://pokeres.bastionbot.org/images/pokemon/2.png"
    },
    {
        id: 3,
        name: "Venosaur",
        image: "https://pokeres.bastionbot.org/images/pokemon/3.png"
    },
    {
        id: 4,
        name: "Charmander",
        image: "https://pokeres.bastionbot.org/images/pokemon/4.png"
    }
]

app.get('/movies', (req, res) => {
    var filtered = movies;
    var nameFilter = "";
    if (req.query.name) {
        nameFilter = req.query.name.toLocaleLowerCase();
        filtered = movies
                    .filter(
                        m => m.name.toLocaleLowerCase()
                                        .indexOf(nameFilter) > -1
                    )
    }
    res.json({
        results: filtered
    })
});

app.get('/movies/:id', (req, res) => {
    var movie = movies.find(m => m.id == req.params.id);
    res.json(movie);
});

app.post('/movies', (req, res) => {
    var movie = movies.find(m => m.id == req.body.id)
    if (movie) {
        movie = req.body;
        movies = movies.filter(m => m.id != req.body.id);
    }
    movies.push(req.body);
    
    res.json(movies);
});

app.delete('/movies/:id', (req, res) => {
    if (req.params.id) {
        movie = req.body;
        movies = movies.filter(m => m.id != req.params.id);
    }
    
    res.json(movies);
});

app.listen(3000, function () {
    console.log('Example app listening on port 3000!');
});